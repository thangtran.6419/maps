package com.ueno.reactnativestarter;

import com.appota.wifichua.sdk.ConnectionModule;
import com.appota.wifichua.sdk.WiFiChuaModule;
import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;

import java.util.ArrayList;
import java.util.List;

public class NativePackage implements ReactPackage {

    public String getName() {
        return "NativePackage";
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return new ArrayList<>();
    }

    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
        ArrayList<NativeModule> modules = new ArrayList<>();
        modules.add(new ConnectionModule(reactContext));
        modules.add(new WiFiChuaModule(reactContext));
        return modules;
    }
}
