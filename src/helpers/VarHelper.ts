
import CryptoJS from 'crypto-js';
import md5 from 'crypto-js/md5';

class VarHelper {
  md5Base64 = (string) => {
    return md5(string).toString(CryptoJS.enc.Base64)
  }
}

export default new VarHelper();
