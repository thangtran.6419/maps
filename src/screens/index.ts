import CodePush from 'react-native-code-push';
import { Navigation } from 'react-native-navigation';
import { codePushConfig } from '../utils/code-push';
import { default as TemplateScreen } from './clone_me/TemplateScreen';
import { default as MapsScreen } from './maps/MapsScreen';
import { default as TestAPIScreen } from './test_api/TemplateScreen';
import { default as FindWifiScreen } from './find_wifi/FindWifiScreen';


export const Screens = new Map();

export const SCREENS = {
  TEMPLATE: 'playground.Template',
  TEST_API: 'playground.TestAPI',
  MAPS: 'playground.Maps',
  FINDWIFI: 'playground.FindWifi'
};

Screens.set(SCREENS.TEMPLATE, CodePush(codePushConfig())(TemplateScreen));
Screens.set(SCREENS.MAPS, MapsScreen);
Screens.set(SCREENS.TEST_API, TestAPIScreen);
Screens.set(SCREENS.FINDWIFI, FindWifiScreen);

export const startApp = () => {
  Navigation.setRoot({
    root: {
      stack: {
        id: 'ROOT_STACK',
        children: [
          {
            component: { name: SCREENS.FINDWIFI },
          },
        ],
      },
    },
  });
};
