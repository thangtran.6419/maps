import React, { Component } from 'react';
import { Text, View, StyleSheet, ActivityIndicator, Image, TouchableOpacity, FlatList, Dimensions, ScrollView } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Button } from '../../components/button/Button';
import { SCREENS } from '..';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import STYLE, { INFO, COLOR } from '../../uikit/constants';
import stores from '../../stores';
import ItemWifiMap from './ItemWifiMap';
const { width, height } = Dimensions.get('window');

interface Props {
  componentId?: any;
  number?: number;
}

const ASPECT_RATIO = (STYLE.WIDTH - 30) / (STYLE.HEIGHT - 300);
const LATITUDE_DELTA = 0.02;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class MapsScreen extends Component<Props, any> {
  map: any;

  constructor(props) {
    super(props);
    this.state = {
      location: { lat: 0, lng: 0 },
      isLoading: true,
      didScan: true,
      wifiDev: [
        {
          nameWifi: 'Wifi Ca Phe Dev 2',
          password: 'Co mat khau'
        },
        {
          nameWifi: 'Wifi Ca Phe Dev 2',
          password: 'Co mat khau'
        },
        {
          nameWifi: 'Wifi Ca Phe Dev 2',
          password: 'Co mat khau'
        }
      ],
    }
  }
  static navigationOptions = ({ header: null })
  static options = {
    topBar: { visible: false }
  };
  static defaultProps = {
    number: 0,
  };

  componentDidMount = async () => {
    const { locationStore } = stores;
    let res = await locationStore.getCurrentPosition();
    this.setState({ location: res, isLoading: false });
  }

  onPress = () => {
    const { number } = this.props;
    Navigation.push(this.props.componentId, {
      component: {
        name: SCREENS.TEST_API,
        passProps: {
          number: number ? number + 1 : 1,
        },
      },
    });
  };

  onRegionChangeComplete = (region) => {
    console.log(region);
    this.setState({ didScan: false });
  }

  navigateToCurrentLocation = () => {
    const { location } = this.state;
    this.map.animateToRegion({
      latitude: parseFloat(location.lat),
      longitude: parseFloat(location.lng),
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
    });
  }

  searchWifi = () => {
    this.setState({ didScan: true });
  }

  render() {
    const { number } = this.props;
    const { location, isLoading, didScan } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <ScrollView scrollEnabled={false}>
          <View style={styles.formContainer}>
            {Boolean(!isLoading) ?
              <View style={{ borderRadius: 10, overflow: 'hidden' }}>
                <MapView
                  ref={ref => { this.map = ref; }}
                  style={{ alignSelf: 'stretch', height: STYLE.HEIGHT - 230, borderRadius: 10 }}
                  initialRegion={{
                    latitude: location.lat,
                    longitude: location.lng,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                  }}
                  showsUserLocation
                  onRegionChangeComplete={(region) => this.onRegionChangeComplete(region)}
                  provider={PROVIDER_GOOGLE}
                />
                <TouchableOpacity onPress={this.navigateToCurrentLocation} style={styles.navigatorIcon} activeOpacity={0.8}>
                  <Image
                    source={require('@assets/icons/navigation.png')}
                    style={{ height: 50, width: 50 }}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
                {Boolean(!didScan) ?
                  <TouchableOpacity onPress={this.searchWifi} style={styles.btnSearch} activeOpacity={0.2}>
                    <Text style={{ color: 'white', fontWeight: '600' }}>Tìm kiếm wifi khu vực này</Text>
                  </TouchableOpacity>
                  :
                  <View />
                }
              </View>
              :
              <ActivityIndicator />
            }
          </View>
        </ScrollView>
        {Boolean(!isLoading) &&
          <View style={{ position: 'absolute', bottom: 15, left: 0 }}>
            <FlatList
              data={this.state.wifiDev}
              renderItem={({ item, index }) => <ItemWifiMap item={item} />}
              keyExtractor={(item, index) => `dfs${index}`}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            />
          </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerText: {
    marginVertical: 20,
  },
  formContainer: {
    backgroundColor: 'white',
    marginHorizontal: 15,
    marginTop: 15,
    width: STYLE.WIDTH - 30,
    marginBottom: 10,
    borderRadius: 10,
    borderWidth: INFO.IS_IOS ? 0 : 1,
    borderColor: COLOR.BORDER,
    shadowColor: 'black',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: { height: 1, width: 0 },
    elevation: 1,
  },
  navigatorIcon: {
    position: 'absolute',
    top: 15,
    left: 10,
  },
  btnSearch: {
    position: 'absolute',
    top: 15,
    alignSelf: 'center',
    borderRadius: 8,
    padding: 10,
    backgroundColor: 'rgb(70,120,243)',
  }
});
