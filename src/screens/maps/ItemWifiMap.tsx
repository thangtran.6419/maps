import React from 'react';
import { View, Text, Image, Dimensions, StyleSheet } from 'react-native';
const { width, height } = Dimensions.get('window');

interface Props {
    item?: any,
}

export default class ItemWifiComponent extends React.Component<Props> {

    render() {
        const { item } = this.props;
        return (
            <View style={style.container}>
                <Text style={style.nameWifi}>{item.nameWifi}</Text>
                <Text style={{ fontSize: 12 }}>{item.password}</Text>
                <View style={style.ViewConnect}>
                    <Text style={{ color: 'white', fontSize: 15 }}>Kết nối</Text>
                </View>
            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        paddingLeft: 20,
        justifyContent: "center",
        width: Math.min(width - 80, 300),
        height: 120,
        borderRadius: 20,
        backgroundColor: 'white',
        flexDirection: 'column',
        marginLeft: 15,
        shadowColor: 'black',
        shadowRadius: 5,
        shadowOpacity: 0.2,
        shadowOffset: { height: 1, width: 0 },
        elevation: 1,
        marginBottom: 1,
    },
    nameWifi: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 15
    },
    ViewConnect: {
        width: 100,
        height: 35,
        backgroundColor: 'rgb(70,120,243)',
        borderRadius: 10,
        marginTop: 5,
        justifyContent: 'center',
        alignItems: 'center'
    }
})