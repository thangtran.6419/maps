import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Button } from '../../components/button/Button';
import { SCREENS } from '..';

interface Props {
  componentId?: any;
  number?: number;
}

export default class TemplateScreen extends Component<Props> {
  static options = {
    topBar: {
      title: {
        text: 'Home',
      },
    },
  };
  static defaultProps = {
    number: 0,
  };

  onPress = () => {
    const { number } = this.props;
    Navigation.push(this.props.componentId, {
      component: {
        name: SCREENS.MAPS,
        passProps: {
          number: number ? number + 1 : 1,
        },
      },
    });
  };

  render() {
    const { number } = this.props;
    return (
      <View style={{ flex: 1, backgroundColor: 'ocean', padding: 15 }}>
        <View>
          <Text style={styles.headerText}>Welcome Home {number || ''}</Text>
        </View>

        <Button onPress={this.onPress} title="Navigate to screen" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerText: {
    marginVertical: 20,
  },
});
