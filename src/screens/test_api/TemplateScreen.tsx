import React, { Component } from 'react';
import { Text, View, StyleSheet, PermissionsAndroid, ScrollView, TouchableOpacity, Platform, Alert } from 'react-native';
import Permissions from 'react-native-permissions'
import { Navigation } from 'react-native-navigation';
import { Button } from '../../components/button/Button';
import { SCREENS } from '..';
import Stores from '../../stores';

interface Props {
  componentId?: any;
  number?: number;
}

export default class TemplateScreen extends Component<Props> {
  static options = {
    topBar: {
      title: {
        text: 'Test API',
      },
    },
  }
  static defaultProps = {
    number: 0,
  }
  state = {
    message: '',
    location: {
      lat: '',
      lng: '',
    },
    hotspots: [],
  }

  componentDidMount = () => {
    const { wifiChuaStore } = Stores;
    if (Platform.OS === 'ios') wifiChuaStore.IOS_loginWithPhone();
  }

  onPress = () => {
    // const { number } = this.props;
    Navigation.push(this.props.componentId, {
      component: {
        name: SCREENS.TEMPLATE,
        // passProps: {
        //   number: number ? number + 1 : 1,
        // },
      },
    });
  };

  getSessionKey = async () => {
    const { wifiChuaStore } = Stores;
    let res = await wifiChuaStore.getSessionKey();
    this.setState({ message: JSON.stringify(res) });
  }

  getCurrentLocation = async () => {
    const { locationStore } = Stores;
    let granted;
    if (Platform.OS === 'android') {
      granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
          title: 'Permission',
          message: 'App needs access to your location so we can let our app be even more awesome',
          buttonPositive: 'OK',
        }
      );
    } else {
      let permission = await Permissions.request('location');
      if (['authorized', 'restricted'].includes(permission)) {
        granted = true;
      }
    }
    if (granted) {
      let res = await locationStore.getCurrentPosition();
      this.setState({ message: JSON.stringify(res), location: res });
    }
  }

  getHotspotsList = async () => {
    const { wifiChuaStore } = Stores;
    const { location } = this.state;
    let res = await wifiChuaStore.getHotspotsList({
      limit: 1,
      location: `${location.lat},${location.lng}`,
      bssids: [],
    })
    this.setState({ hotspots: res.objects });
  }

  getHotspotsListWithBssids = async () => {
    const { wifiChuaStore } = Stores;
    const { location } = this.state;
    let scanedNativeWifi = await wifiChuaStore.scanWifiNative({ lat: 0, lng: 0 });
    scanedNativeWifi = scanedNativeWifi.map(i => i.bssid);
    let res = await wifiChuaStore.getHotspotsList({
      limit: 20,
      location: `${location.lat},${location.lng}`,
      bssids: scanedNativeWifi.join(','),
    })
    this.setState({ hotspots: res.objects });
  }

  connectWifi = async (wifi) => {
    const { wifiChuaStore } = Stores;
    let password;
    try {
      if (wifi.password) password = await wifiChuaStore.decryptPassword(wifi.password);
      let res = await wifiChuaStore.connectWifi(wifi, password);
      this.setState({ message: typeof res === 'string' ? res : JSON.stringify(res) });
    } catch (error) {
      Alert.alert('Connect failed', typeof error === 'string' ? error : error.toString())
    }
  }

  scanWifiNative = async () => {
    const { wifiChuaStore } = Stores;
    let res = await wifiChuaStore.scanWifiNative({ lat: 0, lng: 0 });
    this.setState({ message: typeof res === 'string' ? res : JSON.stringify(res) });
  }

  render() {
    const { number } = this.props;
    return (
      <View style={{ flex: 1, backgroundColor: 'ocean', padding: 15 }}>
        <ScrollView>
          <View>
            <Text onPress={() => this.onPress()} style={styles.headerText}>Welcome Home {number || ''}</Text>
          </View>
          <Button onPress={this.getCurrentLocation} title="Get location" />
          <Button onPress={this.getSessionKey} title="Get session key" />
          <Button onPress={this.getHotspotsList} title="Get hotspot list" />
          <Button onPress={this.getHotspotsListWithBssids} title="Get hotspot list by Bssids" />
          <Button onPress={this.scanWifiNative} title="Scan wifi native" />
          <View>
            <Text style={styles.headerText}>{this.state.message}</Text>
          </View>
          {this.state.hotspots.map((item, index) => {
            return (
              <TouchableOpacity onPress={() => this.connectWifi(item)} style={{ marginBottom: 10 }} key={`hotspot${index}`}>
                {Object.keys(item).map((wifi, i) => {
                  return (
                    <Text key={`${index}${i}`}>{wifi}: {typeof item[wifi] === 'string' ? item[wifi] : JSON.stringify(item[wifi])}</Text>
                  )
                })}
              </TouchableOpacity>
            )
          })}
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  headerText: {
    marginVertical: 20
  }
})