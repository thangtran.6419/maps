import React from 'react';
import { View, Text, Image, Dimensions, StyleSheet } from 'react-native';
const { width, height } = Dimensions.get('window');

export default class HeadComponent extends React.Component {

    render() {
        return (
            <View style={style.container}>
                <View style={style.textView}>
                    <Text style={{ color: 'black', fontSize: 18, fontWeight: '600' }}>Không kết nối</Text>
                    <Text>Chưa có kết nối wifi</Text>
                </View>
                <View style={style.imageView}>
                    <Image style={style.image} source={require('../../image/NoInternet.png')} />
                </View>
            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        justifyContent: 'center',
        width: width - 40,
        height: 100,
        borderRadius: 20,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    textView: {
        paddingLeft: 15,
        flex: 8,
        borderRadius: 20,
        justifyContent: "center"
    },
    imageView: {
        flex: 2,
        borderRadius: 20,
        justifyContent: "center"
    },
    image: {
        width: 40,
        height: 30,
    }

})