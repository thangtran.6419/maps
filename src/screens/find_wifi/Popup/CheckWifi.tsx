import React from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity } from 'react-native';
const { width, height } = Dimensions.get('window');

interface Props {
    componentId?: any;
    number?: number;
    closePopup?: any;
    wifiChoose?: any;
    PopupPass?: any;
}

export default class CheckWifi extends React.Component<Props> {

    render() {
        const { wifiChoose } = this.props
        return (
            <TouchableOpacity activeOpacity={1} onPress={this.props.closePopup} style={style.touchView}>
                <View style={style.container}>
                    <View style={style.headView}>
                        <Text style={{ color: 'black', fontWeight: '600', fontSize: 15 }}>{wifiChoose.name}</Text>
                    </View>
                    <View style={{ flex: 2, flexDirection: 'row' }}>
                        <TouchableOpacity
                            style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}
                            onPress={() => this.props.PopupPass(wifiChoose)}
                        >
                            <View>
                                <Image source={require('../../../image/lock.png')} style={{ width: 25, height: 35 }} />
                            </View>
                            <Text style={style.choice}>Nhập mật khẩu</Text>
                        </TouchableOpacity>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                            <View>
                                <Image source={require('../../../image/question-mark.png')} style={{ width: 35, height: 35 }} />
                            </View>
                            <Text style={style.choice}>Hướng dẫn</Text>
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                            <View>
                                <Image source={require('../../../image/attention.png')} style={{ width: 35, height: 30 }} />
                            </View>
                            <Text style={style.choice}>Báo lỗi</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

const style = StyleSheet.create({
    container: {
        // position: 'absolute',
        // top: -100,
        height: 200,
        width: width - 60,
        borderRadius: 10,
        backgroundColor: 'white',
        borderColor: 'grey',
        borderWidth: 0.2,
        // opacity: 1,
        // left: 30,
        flexDirection: 'column'
    },
    choice: {
        color: 'black',
        fontSize: 12,
        marginTop: 8,
    },
    headView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 0.2,
        borderBottomColor: 'grey',
        marginHorizontal: 10
    },
    touchView: {
        position: 'absolute',
        width,
        height,
        justifyContent: "center",
        alignItems: "center",
    }
})