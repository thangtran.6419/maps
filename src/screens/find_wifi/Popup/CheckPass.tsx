import React from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
const { width, height } = Dimensions.get('window');

interface Props {
    componentId?: any;
    chooseTick?: any;
    closePopup?: any;
    wifiChoose?: any;
    PopupPass?: any;
}

export default class CheckPass extends React.Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            chooseTick: false
        }
    }

    render() {
        const { wifiChoose } = this.props;
        const BK = this.state.chooseTick ? '#1CC936' : '#EEEEEE';
        return (
            <TouchableOpacity activeOpacity={1} onPress={this.props.closePopup} style={style.touchView}>
                <View style={style.container}>
                    <View style={style.headView}>
                        <Text style={style.nameWifi}>{wifiChoose.name}</Text>
                    </View>
                    <View style={style.content}>
                        <TextInput
                            style={style.textInput}
                            placeholder={'Nhập mật khẩu'}
                            onChangeText={text => this.setState({ text })}
                            value={this.state.text}
                        />
                        <View style={style.checkBox}>
                            <TouchableOpacity onPress={() => this.setState({ chooseTick: !this.state.chooseTick })} style={{ flex: 1 }}>
                                <Image source={require('../../../image/tick.png')} style={[style.tick, { backgroundColor: BK }]} />
                            </TouchableOpacity>
                            <View style={{ flex: 8 }}>
                                <Text style={{ fontSize: 12 }}>Chia sẻ wifi & đồng ý điều khoản sử dụng</Text>
                            </View>
                        </View>
                        <TouchableOpacity style={style.viewConnect}>
                            <Text style={{ color: 'white', fontSize: 15, fontWeight: '600' }}>Kết nối</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={this.props.closePopup}
                            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Text>Bỏ qua</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

const style = StyleSheet.create({
    container: {
        position: 'absolute',
        top: width / 2,
        height: 250,
        width: width - 60,
        borderRadius: 10,
        backgroundColor: 'white',
        borderColor: 'grey',
        borderWidth: 0.2,
        opacity: 1,
        left: 30,
        flexDirection: 'column'
    },
    choice: {
        color: 'black',
        fontSize: 12,
        marginTop: 8,
    },
    textInput: {
        flex: 1,
        margin: 30,
        marginTop: 0,
        borderBottomColor: 'grey',
        borderBottomWidth: 0.2,
        width: width - 120
    },
    headView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginHorizontal: 30
    },
    nameWifi: {
        color: 'black',
        fontWeight: '600',
        fontSize: 15
    },
    content: {
        flex: 3,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    checkBox: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: width - 120,
        height: 40,
        marginTop: -30
    },
    tick: {
        backgroundColor: '#EEEEEE',
        width: 20,
        height: 20
    },
    viewConnect: {
        flex: 1,
        width: 250,
        height: 50,
        backgroundColor: '#21F648',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    touchView: {
        position: 'absolute',
        width,
        height,
        justifyContent: "center",
        alignItems: "center",
    }
})