import React from 'react';
import { View, Text, Image, Dimensions, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
const { width, height } = Dimensions.get('window');

interface Props {
    data?: any;
    item?: any;
    ChooseWifi?: any
}

export default class ItemWifiComponent extends React.Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            data: [
                { id: 1, name: 'Appota' },
                { id: 2, name: 'Appota' },
                { id: 3, name: 'Appota' },
                { id: 4, name: 'Appota' }
            ]
        }
    }


    renderItem = (item) => {
        return (
            <TouchableOpacity style={style.oneItem} onPress={() => this.props.ChooseWifi(item)}>
                <View style={style.viewImage}>
                    <Image source={require('../../image/wifi_2_lock.png')} style={style.image} />
                </View>
                <View style={{ flex: 6, justifyContent: 'center' }}>
                    <Text style={{ marginLeft: 20, color: 'black' }}>{item.name}</Text>
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 15, color: 'black', marginTop: 5 }}>...</Text>
                </View>
            </TouchableOpacity>
        )
    }
    render() {
        return (
            <View style={style.container}>
                <FlatList
                    data={this.state.data}
                    renderItem={({ item, index }) => this.renderItem(item)}
                    keyExtractor={(item, index) => `sksahk${index}`}
                />

            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        width: width - 40,
        height: height - 230,
        borderRadius: 20,
        backgroundColor: 'white',
        flexDirection: 'row',
        // backgroundColor: 'red'
    },
    oneItem: {
        margin: 5,
        flexDirection: 'row',
        width: width - 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewImage: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: 30,
        height: 30,
        margin: 10
    }
})