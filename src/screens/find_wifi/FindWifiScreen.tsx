import React, { Component } from 'react';
import { Text, View, StyleSheet, Dimensions } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Button } from '../../components/button/Button';
import { SCREENS } from '..';
import HeadComponent from './HeadComponent';
import ItemWifi from './ItemWifi';
import CheckWifi from './Popup/CheckWifi';
import CheckPass from './Popup/CheckPass';

const { width, height } = Dimensions.get('window')

interface Props {
  componentId?: any;
  number?: number;
  checkPopup?: any;
  checkPopupPass?: any;
}

export default class FindWifiScreen extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      checkPopup: false,
      checkPopupPass: false
    }
  }
  static options = {
    topBar: { visible: false },
    header: null
  };

  static defaultProps = {
    number: 0,
  };

  onPress = () => {
    const { number } = this.props;
    Navigation.push(this.props.componentId, {
      component: {
        name: SCREENS.MAPS,
        passProps: {
          number: number ? number + 1 : 1,
        },
      },
    });
  };

  chooseWifi = (item) => {
    console.log(item)
    this.setState({ checkPopup: true, wifiChoose: item })
  }

  PopupPass = (wifiChoose) => {
    this.setState({ checkPopupPass: true, checkPopup: false })
  }
  render() {
    const { number } = this.props;
    const opacityPopup = this.state.checkPopup || this.state.checkPopupPass ? 0.1 : 1;
    return (
      <View style={{ flex: 1 }}>
        <View style={[styles.container, { opacity: opacityPopup }]}>
          <HeadComponent />
          <Text style={styles.nearly}>Wifi gần đây</Text>
          <ItemWifi ChooseWifi={this.chooseWifi} />

        </View>
        {this.state.checkPopup ?
          <CheckWifi wifiChoose={this.state.wifiChoose} PopupPass={this.PopupPass}
            closePopup={() => this.setState({ checkPopup: false })} /> : null
        }
        {this.state.checkPopupPass ?
          <CheckPass wifiChoose={this.state.wifiChoose}
            closePopup={() => this.setState({ checkPopupPass: false })} /> : null
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F6F6F6',
    padding: 15,
    alignItems: 'center',
    justifyContent: "center"
  },
  nearly: {
    color: 'black',
    marginTop: 20,
    marginBottom: 5,
    fontSize: 15,
    fontWeight: 'bold',
    marginLeft: - width / 2 - 40
  }
});


// onSelectLanguage = {()=> this.handleLanguage()}

// this.props.onSelectLanguage(lang);    