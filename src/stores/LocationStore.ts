
import FusedLocation from 'react-native-fused-location';
import { Platform } from 'react-native';

class Location {
  getCurrentPosition = () => new Promise((resolve, reject) => {
    if (Platform.OS === 'ios') {
      const localeCallback = (locale) => {
        resolve({ lat: locale.coords.latitude, lng: locale.coords.longitude });
      };
      const errorCallback = err => { console.log(err); reject(err); };
      const options = { enableHighAccuracy: false, timeout: 15000, maximumAge: 3600000 };
      global.navigator.geolocation.getCurrentPosition(localeCallback, errorCallback, options);
    } else {
      FusedLocation.getFusedLocation().then((location) => {
        resolve({ lat: location.latitude, lng: location.longitude });
      })
        .catch((err) => {
          reject(err);
        });
    }
  });
}

export default new Location();