import { Platform, NativeModules } from 'react-native';
import VarHelper from '../helpers/VarHelper';
import { INFO } from '../uikit/constants';

const userData = {
  phoneNumber: '0335815969',
}

const OS_VERSION = '9';
const DEVICE_OS = Platform.OS;
const DEVICE_NAME = 'Samsung S10';
const BUNDLE_ID = 'com.wallet.apt';

const ConnectionModule = Platform.select({
  ios: NativeModules.ConnectionModule,
  android: NativeModules.ConnectionModule,
});

const WiFiChuaModule = Platform.select({
  ios: NativeModules.WiFiChuaModule,
  android: NativeModules.WiFiChuaModule,
})

const WIFICHUA_HOST = 'https://appwifi-dev.wifichua.co';
const APIS = {
  GET_SESSION_KEY: '/v2/users',
  GET_HOTSPOTS: '/v2/hotspots',
}

class WifiChua {
  sessionId = '';

  makeHeaders = async (request_method, uri, data, session_id) => {
    let objHeaders = {
      "User-Agent": `${BUNDLE_ID}/${INFO.VER_CODEPUSH}/${INFO.BUILD_CODEPUSH} \"${DEVICE_NAME} ${DEVICE_OS} ${OS_VERSION}\"`,
      "Content-Type": request_method === 'POST' ? "application/json" : "text/plain",
      "Date": new Date().getTime(),
      "API-VERSION": "2.1.0",
      "X-S-Key": "AW",
      "Content-MD5": data ? VarHelper.md5Base64(JSON.stringify(data)) : '',
      "X-Request-Id": Math.floor(Math.random() * 1000000),
    }
        // "Session-Id": "345f63feab8d44631349d1c07b094c2699c87209c3f88d40ab376bb2b1f5b82d",
    let plainTextArr = [request_method, objHeaders["Content-MD5"], objHeaders["Content-Type"], objHeaders["X-Request-Id"], uri, objHeaders["Date"]];
    console.log('plainTextArr', plainTextArr.join(','))
    let encrypted = await WiFiChuaModule.encrypt('request', plainTextArr.join(','), session_id || "");
    objHeaders["Authorization"] = `APIAuth ${encrypted}`;
    return objHeaders
  }

  decryptResponse = async (type, encryptedText) => {
    let decrypted = await WiFiChuaModule.decrypt(type, encryptedText, this.sessionId)
    return decrypted;
  }

  getSessionKey = async () => {
    // let userData = userStore.getProps('userData');
    let data = { phone_number: userData.phoneNumber }
    let headers = await this.makeHeaders('POST', APIS.GET_SESSION_KEY, data, null);
    const response = await fetch(WIFICHUA_HOST + APIS.GET_SESSION_KEY, {
      method: 'POST',
      timeout: 3000,
      headers: headers,
      body: JSON.stringify(data),
    });
    const res = await response.json();
    this.sessionId = res.object.session_key;
    return res;
  }

  getHotspotsList = async ({ limit, location, bssids }) => {
    let radius = 2;
    let paramsObj = { limit, location, bssids, radius, map: 'true' };
    let paramsArr = [];
    for (let key in paramsObj) {
      if (!key) continue;
      if (paramsObj[key] !== undefined) {
        paramsArr.push(`${key}=${paramsObj[key]}`)
      }
    }
    let encrypted = await WiFiChuaModule.encrypt('query', paramsArr.join('&'), this.sessionId);
    let url = APIS.GET_HOTSPOTS + `?data=${encrypted}`;
    let headers = await this.makeHeaders('GET', url, null, null);
    headers["Session-Id"] = this.sessionId;
    delete headers["Content-MD5"];
    const response = await fetch(WIFICHUA_HOST + url, {
      timeout: 3000,
      headers: headers,
    });
    const res = await response.text();
    let decryptedRes = await this.decryptResponse('response', res);
    return JSON.parse(decryptedRes);
  }

  decryptPassword = async (encryptedPass) => {
    let password = await WiFiChuaModule.decrypt('password', encryptedPass, this.sessionId);
    return password;
  }

  scanWifiNative = (cords) => new Promise((resolve, reject) => {
    ConnectionModule.scan(cords.lat, cords.lng, (data) => {
      resolve(data);
    });
  })

  connectWifi = (wifi, password) => new Promise(async (resolve, reject) => {
    if (Platform.OS === 'ios') {
      ConnectionModule.connect(wifi.ssid, wifi.bssid, password, 365);
      resolve('connecting');
      return;
    }
    const scanedWifi = await this.scanWifiNative({ lat: 0, lng: 0 });
    let wifiNative = scanedWifi.find(i => i.ssid === wifi.ssid);
    console.log('matched wifi', wifiNative);
    if (!wifiNative) reject('Selected wifi is too far from you')
    else if (wifiNative.state === 'connected') reject('You already connect to this wifi')
    else {
      ConnectionModule.connect(wifi.ssid, password, wifiNative ? wifiNative.mode : 'WPA', (status) => {
        console.log('connect status', status);
        resolve(status)
      })
    }
  })

  IOS_loginWithPhone = async () => {
    let isLogin = await WiFiChuaModule.isLoggedIn();
    console.log('IOS_loginWithPhone', isLogin);
    if (!isLogin) {
      let res = await WiFiChuaModule.loginWithPhoneNumber('0335815969')
      console.log('loginWithPhoneNumber', res);
      // this.props.wifiChuaStore.getAuthString();
    }
  }
}

const WifiChuaStore = new WifiChua();
export default WifiChuaStore;