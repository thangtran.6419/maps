import { Platform, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

const MUATHE = false; // ẩn mua thẻ

export let COLOR = {
  MAIN: MUATHE ? '#1d82d2' : '#449D47', // thay thế cho HIGHLIGHT, ngắn gọn và dễ hiểu hơn
  HIGHLIGHT: MUATHE ? '#1d82d2' : '#449D47', // DEPRECATED, chuyển dần sang dùng MAIN
  //
  GREY: '#C7C7CD',
  RED: '#FF0000',
  WHITE: '#fff',
  YELLOW: '#ffff00',
  GREEN: '#449D47',
  PURPLE: '#551A8B',
  BLUE: '#2196F3',
  NEW_BLUE: '#12579E',
  ORANGE: '#e15936', //'rgba(250,156,40,1)',
  BLACK: '#000000',
  MAIN_TEXT: '#333',
  SUB_TEXT: '#666',
  SUB_TEXT_LIGHT: '#9b9b9b',
  BG: '#f6f6f6',
  BG_GREY: '#f4f3f9',
  BORDER: '#e9e9e9',
  TITLE_BOX: '#6D6D72',
};

export let STYLE = {
  WIDTH: width,
  // WIDTH: DeviceInfo.isTablet() ? width * 0.55 : width,
  HEIGHT: height,
  HEADER_HEIGHT: Platform.OS === 'ios' ? 44 : 54,
  BORDERWIDTH: 1,
  COLOR,
};

export const INFO = {
  VERSION               : '9.0',
  VER_CODEPUSH          : '4.8.0',
  BUILD_CODEPUSH        : 17,
  IS_ANDROID            : Platform.OS === 'android',
  IS_ANDROID_UNDER44    : Platform.OS === 'android' && Platform.Version < 21,
  IS_ANDROID_5_AND_BELOW: Platform.OS === 'android' && Platform.Version <= 21,
  IS_IOS                : Platform.OS === 'ios',
  IS_MUATHE             : MUATHE,
  SCHEMA_HOST           : MUATHE ? 'muathe://' : 'appotapay://',
  CURRENCY              : MUATHE ? 'đ' : 'vnđ',
  MONEY                 : MUATHE ? 'tiền' : 'tiền',
  FORCE_ACCKIT          : true,
  IS_IOS12              : false, // Platform.OS === 'ios' && DeviceInfo.getSystemVersion().indexOf('12.') === 0,
};

export const PRICE = {
  GEM: 'gem',
  GEMS: 'Gem',
  VND: 'vnđ',
};

export default STYLE;
