//
//  WiFiChuaSDK.h
//  WiFiChuaSDK
//
//  Created by Steven on 6/10/19.
//  Copyright © 2019 Appota. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WiFiChuaModule.h"

@interface WiFiChuaSDK : NSObject

+ (void)setDebugMode:(BOOL)isDebugMode;
+ (BOOL)isLoggedIn;
+ (void)loginWithPhoneNumber:(NSString*)phoneNumber completion:(void (^)(BOOL result))completion;
+ (void)setupWithWiFiLabel:(NSString*)wifiLabel;

@end
