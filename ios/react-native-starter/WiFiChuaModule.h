//
//  WiFiChuaModule.h
//  WiFiChuaSDK
//
//  Created by Steven on 6/11/19.
//  Copyright © 2019 Appota. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

NS_ASSUME_NONNULL_BEGIN

@interface WiFiChuaModule : NSObject <RCTBridgeModule>

// encrypt:(NSString*)type plainText:(NSString*)plainText sessionID:(NSString*)sessionID resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject
// decrypt:(NSString*)type encryptedString:(NSString*)encryptedString sessionID:(NSString*)sessionID resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject
// loginWithPhoneNumber:(NSString*)phoneNumber resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
// isLoggedIn:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject

@end

NS_ASSUME_NONNULL_END
