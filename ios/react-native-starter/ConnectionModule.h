//
//  ConnectionModule.h
//  WiFiChuaSDK
//
//  Created by Steven on 6/12/19.
//  Copyright © 2019 Appota. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface ConnectionModule : NSObject <RCTBridgeModule>

// connect:(NSString*)ssid bssid:(NSString*)bssid password:(NSString*)password lifeTimeInDays:(NSInteger)lifeTimeInDays
// forget:(NSString*)ssid bssid:(NSString*)bssid completion:(RCTResponseSenderBlock)completion)
// listSavedNetworks:(RCTResponseSenderBlock)completion)

@end
